from flask import Flask, request, jsonify
# from flask_cors import CORS, cross_origin
import os

app = Flask(__name__)
# CORS(app)

# @app.route("/hello")
# def hello_world():
#     return "<p>Hello, World!</p>"


UPLOAD_FOLDER = 'data'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
@app.route('/upload', methods=['POST'])
def upload_file():
    try:
        if 'file' not in request.files:
            return jsonify({'error': 'No file part'}), 400

        file = request.files['file']

        if file.filename == '':
            return jsonify({'error': 'No selected file'}), 400

        # Simpan file ke dalam folder uploads
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

        return jsonify({'message': 'File uploaded successfully'}), 200

    except Exception as e:
        print(f'Error handling upload: {e}')
        return jsonify({'error': 'Internal server error'}), 500

if __name__ == "__main__":
    app.run(debug=True)