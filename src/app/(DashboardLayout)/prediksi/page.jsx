"use client";

import { useState } from "react";
import PageContainer from '@/app/(DashboardLayout)/components/container/PageContainer';
import DashboardCard from '@/app/(DashboardLayout)/components/shared/DashboardCard';
import UploadForm from "./uploadFile";
import NumberForm from "./formNumber";
import FinalForm from "./finalForm";

const HomePage = () => {
  const [step, setStep] = useState(0);

  const handleNext = () => {
    setStep((prevStep) => prevStep + 1);
  };

  const handleReset = () => {
    setStep(0);
  };

  return (
    <PageContainer title="Prediksi Kopi" description="this is Sample page">
      <DashboardCard title="Prediksi">
        {step === 0 && <UploadForm onNext={handleNext} />}
        {step === 1 && <NumberForm onNext={handleNext} />}
        {step === 2 && <FinalForm onReset={handleReset} />}
      </DashboardCard>
    </PageContainer>
  );
};

export default HomePage;
