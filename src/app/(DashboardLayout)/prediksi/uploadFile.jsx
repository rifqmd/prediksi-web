"use client";

import React, { useState, useCallback } from "react";
import { useDropzone } from "react-dropzone";
import {
  Container,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Box,
  Typography,
  Paper,
  LinearProgress,
} from "@mui/material";

const UploadForm = ({ onNext }) => {
  const [fileType, setFileType] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploading, setUploading] = useState(false); // State untuk menunjukkan proses upload

  const handleFileTypeChange = (event) => {
    setFileType(event.target.value);
  };

  const onDrop = useCallback((acceptedFiles) => {
    if (acceptedFiles.length > 0) {
      setSelectedFile(acceptedFiles[0]);
    }
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    // Lakukan proses upload file di sini
    console.log("File Type:", fileType);
    console.log("Selected File:", selectedFile);
    // onNext();

    if (!selectedFile) {
      alert("Please select a file.");
      return;
    }

    const formData = new FormData();
    formData.append("file", selectedFile);

    try {
      setUploading(true); // Mulai proses upload
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/upload`, {
        method: 'POST',
        body: formData,
      });

      if (!response.ok) {
        throw new Error('Failed to upload file');
      }

      // Handle response jika perlu
      const data = await response.json();
      console.log('File uploaded successfully:', data);

      // Reset form jika perlu
      setSelectedFile(null);

    } catch (error) {
      console.error('Error uploading file:', error.message);
    } finally {
      setUploading(false); // Selesai proses upload
    }
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: fileType === "csv" ? ".csv" : ".xlsx",
  });

  return (
    <Container maxWidth="sm">
      <form onSubmit={handleSubmit}>
        <Box sx={{ my: 2 }}>
          <FormControl fullWidth>
            <InputLabel id="file-type-label">File Type</InputLabel>
            <Select
              labelId="file-type-label"
              id="file-type"
              value={fileType}
              label="File Type"
              onChange={handleFileTypeChange}
            >
              <MenuItem value="csv">CSV</MenuItem>
              <MenuItem value="xlsx">XLSX</MenuItem>
            </Select>
          </FormControl>
        </Box>

        <Box sx={{ my: 2 }}>
          <Paper
            {...getRootProps()}
            sx={{
              border: "2px dashed #aaa",
              padding: 2,
              textAlign: "center",
              cursor: "pointer",
              backgroundColor: isDragActive ? "#f0f0f0" : "#fafafa",
            }}
          >
            <input {...getInputProps()} />
            <Typography variant="body1">
              {isDragActive
                ? "Drop the files here..."
                : "Drag & drop a file here, or click to select a file"}
            </Typography>
          </Paper>
          {selectedFile && (
            <Box sx={{ mt: 2 }}>
              <Typography variant="body2">
                Selected File: {selectedFile.name}
              </Typography>
            </Box>
          )}
        </Box>

        <Box sx={{ my: 2 }}>
          <Button type="submit" variant="contained" color="primary" fullWidth>
            Proses
          </Button>
        </Box>
        {/* preses loading */}
        {uploading && (
          <Box sx={{ width: '100%', marginTop: 2 }}>
            <LinearProgress />
          </Box>
        )}

        {/* next step */}
        <Box sx={{ my: 2 }}>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            onClick={onNext}
          >
            Selanjutnya
          </Button>
        </Box>
      </form>
    </Container>
  );
};

export default UploadForm;
