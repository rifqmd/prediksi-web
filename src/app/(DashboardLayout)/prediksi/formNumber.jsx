"use client";

import { useState } from "react";
import { Container, TextField, Box, Button } from "@mui/material";

const NumberForm = ({ onNext }) => {
  const [number, setNumber] = useState();
  const handleChange = (event) => {
    const value = parseInt(event.target.value, 10);
    if (!isNaN(value) && value >= 50 && value <= 90) {
      setNumber(value);
    } else if (event.target.value === "") {
      setNumber("");
    }
  };

  return (
    <Container maxWidth="sm">
      <Box sx={{ my: 2 }}>
        <TextField
          fullWidth
          type="number"
          label="Training Set (70-90)"
          value={number}
          onChange={handleChange}
          inputProps={{ min: 70, max: 90, step: 10 }}
        />
      </Box>
      <Box sx={{ my: 2 }}>
        <TextField
          fullWidth
          type="number"
          label="Testing Set"
          // value={100 - number}
          value={number === "" ? "" : 100 - number}
          disabled
        />
      </Box>
      <Box sx={{ my: 2 }}>
        <Button variant="contained" color="primary" fullWidth onClick={onNext}>
          Selanjutnya
        </Button>
      </Box>
    </Container>
  );
};

export default NumberForm;
