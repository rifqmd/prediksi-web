"use client";

import React from "react";
import { Container, Typography, Box, Button } from "@mui/material";

const FinalForm = ({ onReset }) => {
  return (
    <Container maxWidth="sm">
      <Box sx={{ my: 2 }}>
        <Typography variant="h5">Proses selesai!</Typography>
      </Box>
      <Box sx={{ my: 2 }}>
        <Button variant="contained" color="primary" fullWidth onClick={onReset}>
          Mulai Lagi
        </Button>
      </Box>
    </Container>
  );
};

export default FinalForm;
